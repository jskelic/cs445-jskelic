package main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public class AdvertisingDirectory implements Serializable {
	private static final long serialVersionUID = 2137852326864454714L;
	public static final int MAX_SPONSORS_PER_CATEGORY = 10;
	public static final int MAX_SPONSORS_FOR_HOME_PAGE = 5;

	private Category currentCategory;
	private ArrayList<Advertiser> allAdvertisers;

	public AdvertisingDirectory() {
		currentCategory = new Category(null, "Home");
		allAdvertisers = new ArrayList<Advertiser>();
	}

	public Category getCurrentCategory() {return currentCategory;}
	public ArrayList<Advertiser> getAllAdvertisers() {return allAdvertisers;}

	public Category getHomeCategory() {
		Category category = currentCategory;
		while(category.getParentCategory() != null)
			category = category.getParentCategory();
		return category;
	}

	public Advertiser getAdvertiser(UUID uid) {
		for(Advertiser advertiser : allAdvertisers)
			if(uid.equals(advertiser.getUid()))
				return advertiser;
		return null;
	}

	public void goToSubcategory(String name) {
		for(Category subcategory : currentCategory.getSubcategories())
			if(name.equals(subcategory.getName())) {
				currentCategory = subcategory;
				return;
			}
		throw new NoSuchCategoryException();
	}

	public void goToParentCategory() {
		if(currentCategory.getParentCategory() == null)
			throw new NoSuchCategoryException();
		currentCategory = currentCategory.getParentCategory();
	}

	public void addSubcategory(String name) {
		currentCategory.addSubcategory(name);
	}

	public void deleteSubcategory(String name) {
		Category deletedCategory = currentCategory.deleteSubcategory(name);
		if(deletedCategory == null)
			throw new NoSuchCategoryException();
		ArrayList<Advertiser> advertisersToBeDeleted = new ArrayList<Advertiser>();
		for(Advertiser advertiser : allAdvertisers) {
			if(deletedCategory.getAdvertisers().contains(advertiser))
				advertiser.subtractAdvertiserAmount();
			if(deletedCategory.getSponsors().contains(advertiser))
				advertiser.subtractSponsorAmount();
			if(Math.abs(advertiser.getDues()) < 0.000001)
				advertisersToBeDeleted.add(advertiser);
		}
		for(Advertiser advertiser : advertisersToBeDeleted)
			allAdvertisers.remove(advertiser);
	}

	public Advertiser addAvertiser(Advertiser advertiser) {
		if(currentCategory.getParentCategory() == null)
			throw new CannotAdvertiseHomePageException();
		if(allAdvertisers.contains(advertiser)) {
			for(Advertiser ad : allAdvertisers)
				if(ad.equals(advertiser))
					advertiser = ad;
		}
		else
			allAdvertisers.add(advertiser);
		currentCategory.addAdvertiser(advertiser);
		advertiser.addAdvertiserAmount();
		return advertiser;
	}

	public void deleteAdvertiser(UUID uid) {
		Advertiser deletedAdvertiser = currentCategory.deleteAdvertiser((getAdvertiser(uid)));
		if(deletedAdvertiser == null)
			throw new NoSuchAdvertiserException();
		deletedAdvertiser.subtractAdvertiserAmount();
		if(Math.abs(deletedAdvertiser.getDues()) < 0.000001)
			allAdvertisers.remove(deletedAdvertiser);
	}

	public Advertiser addSponsor(Advertiser sponsor) {
		boolean homeSponsor;
		if(currentCategory.getParentCategory() == null)
			homeSponsor = true;
		else
			homeSponsor = false;

		if(!homeSponsor && currentCategory.getSponsors().size() == MAX_SPONSORS_PER_CATEGORY)
			return null;
		else if(homeSponsor && currentCategory.getSponsors().size() == MAX_SPONSORS_FOR_HOME_PAGE)
			return null;

		if(allAdvertisers.contains(sponsor)) {
			for(Advertiser spon : allAdvertisers)
				if(spon.equals(sponsor))
					sponsor = spon;
		}
		else
			allAdvertisers.add(sponsor);

		currentCategory.addSponsor(sponsor);
		if(homeSponsor)
			sponsor.addHomeSponsorAmount();
		else
			sponsor.addSponsorAmount();
		return sponsor;
	}

	public void deleteSponsor(UUID uid) {
		Advertiser deletedSponsor = currentCategory.deleteSponsor(getAdvertiser(uid));
		if(deletedSponsor == null)
			throw new NoSuchSponsorException();
		if(currentCategory.getParentCategory() == null)
			deletedSponsor.subtractHomeSponsorAmount();
		else
			deletedSponsor.subtractSponsorAmount();
		if(Math.abs(deletedSponsor.getDues()) < 0.000001)
			allAdvertisers.remove(deletedSponsor);
	}





	public class NoSuchCategoryException extends RuntimeException {
		private static final long serialVersionUID = -7328143139363332146L;
	}

	public class CannotAdvertiseHomePageException extends RuntimeException {
		private static final long serialVersionUID = 7080132969553729451L;
	}

	public class NoSuchAdvertiserException extends RuntimeException {
		private static final long serialVersionUID = -5446140082496731350L;
	}

	public class NoSuchSponsorException extends RuntimeException {
		private static final long serialVersionUID = -4990942650036327561L;
	}
}