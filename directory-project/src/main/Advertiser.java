package main;

import java.io.Serializable;
import java.util.UUID;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Advertiser implements Serializable {
	private static final long serialVersionUID = -4139151804357049279L;
	public static final double BASIC_LISTING_PRICE = 1.99;
	public static final double ADDITIONAL_LISTING_PRICE = 0.99;
	public static final double CATEGORY_SPONSORSHIP_PRICE = 3.99;
	public static final double HOME_PAGE_SPONSORSHIP_PRICE = 4.99;

	@XmlAttribute(name="name")
	private String name;
	@XmlAttribute(name="dues", required=false)
	private double dues;
	@XmlAttribute(name="logo")
	private String logo;
	@XmlAttribute(name="numAds", required=false)
	private int numAds;
	@XmlAttribute(name="uid", required=false)
	private UUID uid;

	public Advertiser() {
		name = "";
		dues = 0;
		logo = "";
		numAds = 0;
		uid = UUID.randomUUID();
	}

	public Advertiser(String name, String logo) {
		this.name = name;
		dues = 0;
		this.logo = logo;
		numAds = 0;
		uid = UUID.randomUUID();
	}

	public String getName() {return name;}
	public double getDues() {return dues;}
	public String getLogo() {return logo;}
	public UUID getUid() {return uid;}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Advertiser))
			return false;
		Advertiser advertiser = (Advertiser)obj;
		return name.equals(advertiser.getName()) && logo.equals(advertiser.getLogo());
	}

	@Override
	public int hashCode() {
		return name.hashCode() + logo.hashCode();
	};

	public void addAdvertiserAmount() {
		dues += numAds++ == 0 ? BASIC_LISTING_PRICE : ADDITIONAL_LISTING_PRICE;
	}

	public void subtractAdvertiserAmount() {
		dues -= --numAds == 0 ? BASIC_LISTING_PRICE : ADDITIONAL_LISTING_PRICE;
	}

	public void addSponsorAmount() {
		dues += CATEGORY_SPONSORSHIP_PRICE;
	}

	public void subtractSponsorAmount() {
		dues -= CATEGORY_SPONSORSHIP_PRICE;
	}

	public void addHomeSponsorAmount() {
		dues += HOME_PAGE_SPONSORSHIP_PRICE;
	}

	public void subtractHomeSponsorAmount() {
		dues -= HOME_PAGE_SPONSORSHIP_PRICE;
	}
}