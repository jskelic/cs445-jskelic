package main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Category implements Serializable {
	private static final long serialVersionUID = 4100210506534725090L;

	@XmlAttribute(name="name")
	private String name;
	@XmlTransient
	private Category parentCategory;
	@XmlElement(name="subcategories")
	private ArrayList<Category> subcategories;
	@XmlTransient
	private ArrayList<Advertiser> advertisers;
	@XmlTransient
	private ArrayList<Advertiser> sponsors;

	public Category() {
		name = "";
		parentCategory = null;
		subcategories = new ArrayList<Category>();
		advertisers = new ArrayList<Advertiser>();
		sponsors = new ArrayList<Advertiser>();
	}

	public Category(Category parentCategory, String name) {
		this.parentCategory = parentCategory;
		this.name = name;
		subcategories = new ArrayList<Category>();
		advertisers = new ArrayList<Advertiser>();
		sponsors = new ArrayList<Advertiser>();
	}

	public String getName() {return name;}
	public Category getParentCategory() {return parentCategory;}
	public ArrayList<Category> getSubcategories() {return subcategories;}
	public ArrayList<Advertiser> getAdvertisers() {return advertisers;}
	public ArrayList<Advertiser> getSponsors() {return sponsors;}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Category))
			return false;
		Category category = (Category)obj;
		if(parentCategory == null && category.getParentCategory() != null)
			return false;
		else if(parentCategory != null && category.getParentCategory() == null)
			return false;
		else if(parentCategory == null && category.getParentCategory() == null)
			return name.equals(category.getName());
		else
			return name.equals(category.getName()) && parentCategory.equals(category.getParentCategory());
	}

	public Set<Advertiser> getFeaturedAdvertisers() {
		Set<Advertiser> featuredAdvertisers = new HashSet<Advertiser>();
		featuredAdvertisers.addAll(sponsors);
		if(parentCategory != null)
			featuredAdvertisers.addAll(parentCategory.getFeaturedAdvertisers());
		return featuredAdvertisers;
	}

	public void addSubcategory(String name) {
		Category subCategory = new Category(this, name);
		if(subcategories.contains(subCategory))
			throw new SubcategoryAlreadyExistsException();
		subcategories.add(subCategory);
	}

	public Category deleteSubcategory(String name) {
		Category subcategory = null;
		for(Category childCategory : subcategories)
			if(name.equals(childCategory.getName())) {
				subcategory = childCategory;
				break;
			}
		if(subcategories.remove(subcategory))
			return subcategory;
		else
			return null;
	}

	public void addAdvertiser(Advertiser advertiser) {
		if(advertisers.contains(advertiser))
			throw new AdvertiserAlreadyExistsException();
		advertisers.add(advertiser);
	}

	public Advertiser deleteAdvertiser(Advertiser advertiser) {
		for(Advertiser ad : advertisers)
			if(advertiser.equals(ad)) {
				advertiser = ad;
				break;
			}
		if(advertisers.remove(advertiser))
			return advertiser;
		else
			return null;
	}

	public void addSponsor(Advertiser sponsor) {
		if(getFeaturedAdvertisers().contains(sponsor))
			throw new SponsorAlreadyExistsException();
		sponsors.add(sponsor);
	}

	public Advertiser deleteSponsor(Advertiser sponsor) {
		for(Advertiser spon : sponsors)
			if(sponsor.equals(spon)) {
				sponsor = spon;
				break;
			}
		if(sponsors.remove(sponsor))
			return sponsor;
		else
			return null;
	}





	public class SubcategoryAlreadyExistsException extends RuntimeException {
		private static final long serialVersionUID = -2764685806233207823L;
	}

	public class AdvertiserAlreadyExistsException extends RuntimeException {
		private static final long serialVersionUID = 7733647517753620172L;
	}

	public class SponsorAlreadyExistsException extends RuntimeException {
		private static final long serialVersionUID = -4765228716548445886L;
	}
}