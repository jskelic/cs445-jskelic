package delivery;

import java.util.ArrayList;
import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import main.Advertiser;
import main.Category;

@Path("admin")
public class Admin extends Ad {
	@GET
	@Path("tree")
	@Produces(MediaType.APPLICATION_XML)
	public Category getDirectoryTree() {
		return advertisingDirectory.getHomeCategory();
	}

	@GET
	@Path("ad")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Advertiser> getAllAdvertisers() {
		return advertisingDirectory.getAllAdvertisers();
	}

	@GET
	@Path("ad/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Advertiser getAdvertiser(@PathParam("uid") String uid) {
		return advertisingDirectory.getAdvertiser(UUID.fromString(uid));
	}

	@GET
	@Path("cdl/ad")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Advertiser> getCurrentCategoryAdvertisers() {
		return advertisingDirectory.getCurrentCategory().getAdvertisers();
	}

	@DELETE
	@Path("cdl/ad/{uid}")
	public void deleteAdvertiser(@PathParam("uid") String uid) {
		advertisingDirectory.deleteAdvertiser(UUID.fromString(uid));
		storageMechanism.save(advertisingDirectory);
	}

	@GET
	@Path("cdl/spon")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Advertiser> getCurrentCategorySponsors() {
		return advertisingDirectory.getCurrentCategory().getSponsors();
	}

	@DELETE
	@Path("cdl/spon/{uid}")
	public void deleteSponsor(@PathParam("uid") String uid) {
		advertisingDirectory.deleteSponsor(UUID.fromString(uid));
		storageMechanism.save(advertisingDirectory);
	}

	@POST
	@Path("cdl/cat")
	@Consumes(MediaType.TEXT_PLAIN)
	public void addSubcategory(String name) {
		advertisingDirectory.addSubcategory(name);
		storageMechanism.save(advertisingDirectory);
	}

	@DELETE
	@Path("cdl/cat/{subcat}")
	public void deleteSubcategory(@PathParam("subcat") String subcat) {
		advertisingDirectory.deleteSubcategory(subcat);
		storageMechanism.save(advertisingDirectory);
	}
}