package delivery;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import main.Advertiser;

@Path("ad")
public class Ad extends User {
	@POST
	@Path("cdl/ad")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addAvertiser(Advertiser advertiser) {
		Advertiser addedAdvertiser = advertisingDirectory.addAvertiser(advertiser);
		storageMechanism.save(advertisingDirectory);
		return addedAdvertiser.getUid().toString();
	}

	@POST
	@Path("cdl/spon")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addSponsor(Advertiser sponsor) {
		Advertiser addedSponsor = advertisingDirectory.addSponsor(sponsor);
		storageMechanism.save(advertisingDirectory);
		return addedSponsor.getUid().toString();
	}
}