package delivery;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import main.*;
import storage.*;

@Path("user")
public class User {
	protected StorageMechanism storageMechanism;
	protected AdvertisingDirectory advertisingDirectory;

	public User() {
		storageMechanism = new MockDatabase();
		advertisingDirectory = storageMechanism.restore();
	}

	@GET
	@Path("cdl")
	@Produces(MediaType.APPLICATION_JSON)
	public Category getCurrentCategory() {
		return advertisingDirectory.getCurrentCategory();
	}

	@PUT
	@Path("cdl")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Category goToSubcategory(String name) {
		advertisingDirectory.goToSubcategory(name);
		storageMechanism.save(advertisingDirectory);
		return advertisingDirectory.getCurrentCategory();
	}

	@PUT
	@Path("cdl/parent")
	@Produces(MediaType.APPLICATION_JSON)
	public Category goToParentCategory() {
		advertisingDirectory.goToParentCategory();
		storageMechanism.save(advertisingDirectory);
		return advertisingDirectory.getCurrentCategory();
	}

	@GET
	@Path("cdl/ads")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrentCategoryAdvertisersNames() {
		String out = "[";
		boolean first = true;
		for(Advertiser advertiser : advertisingDirectory.getCurrentCategory().getAdvertisers()) {
			if(!first)
				out += ",";
			out += "{\"name\":\"";
			out += advertiser.getName();
			out += "\"}";
			first = false;
		}
		out += "]";
		return out.toString();
	}

	@GET
	@Path("cdl/featads")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrentCategorySponsorsNamesAndLogos() {
		String out = "[";
		boolean first = true;
		for(Advertiser advertiser : advertisingDirectory.getCurrentCategory().getFeaturedAdvertisers()) {
			if(!first)
				out += ",";
			out += "{\"name\":\"";
			out += advertiser.getName();
			out += "\",\"logo\":\"";
			out += advertiser.getLogo();
			out += "\"}";
			first = false;
		}
		out += "]";
		return out.toString();
	}
}