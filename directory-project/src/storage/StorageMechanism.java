package storage;

import main.AdvertisingDirectory;

public interface StorageMechanism {
	void save(AdvertisingDirectory advertisingDirectory);
	AdvertisingDirectory restore();
}