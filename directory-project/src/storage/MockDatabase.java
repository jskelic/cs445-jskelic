package storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import main.AdvertisingDirectory;

public class MockDatabase implements StorageMechanism {
	public static final String FILE = System.getProperty("user.home") + "/Desktop/Jusuf-Skelic-PR-db.txt";

	@Override
	public void save(AdvertisingDirectory advertisingDirectory) {
		try {
			ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(new File(FILE)));
			outputStream.writeObject(advertisingDirectory);
			outputStream.close();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public AdvertisingDirectory restore() {
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(new File(FILE)));
			AdvertisingDirectory advertisingDirectory = (AdvertisingDirectory)inputStream.readObject();
			inputStream.close();
			return advertisingDirectory;
		} catch (ClassNotFoundException | FileNotFoundException e) {
			return new AdvertisingDirectory();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}