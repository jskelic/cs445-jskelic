package driver;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import main.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import storage.MockDatabase;
import com.jayway.restassured.http.ContentType;

@Test(singleThreaded=true)
public class AutomatedTestScript {
	private static final String BASE_URL = "http://localhost:8080/Jusuf-Skelic-PR/";

	@BeforeClass
	public void beforeClass() throws IOException {
		File dbFile = new File(MockDatabase.FILE);
		if(dbFile.exists()) {
			dbFile.renameTo(new File(MockDatabase.FILE + ".tmp"));
			dbFile.delete();
		}
	}

	@AfterClass
	public void afterClass() throws IOException {
		File dbFile = new File(MockDatabase.FILE);
		File tempFile = new File(MockDatabase.FILE + ".tmp");
		dbFile.delete();
		if(tempFile.exists()) {
			tempFile.renameTo(dbFile);
			tempFile.delete();
		}
	}

	@Test(priority=1)
	public void testGetInitialCdl() throws JSONException {
		Reporter.log("1. The initial directory structure is \"Home\" with no subcategories. The CDL is \"Home.\"");

		JSONObject actual = new JSONObject(get(BASE_URL + "user/cdl").asString());
		JSONObject expected = new JSONObject()
		.put("name", "Home")
		.put("subcategories", new ArrayList<Category>());

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=2)
	public void testCreateLodgingCategory() throws JSONException {
		Reporter.log("2. The directory structure is \"Home\" with subcategory \"Lodging.\"");

		ArrayList<JSONObject> subcategories = new ArrayList<JSONObject>();
		subcategories.add(new JSONObject()
		.put("name", "Lodging")
		.put("subcategories", new ArrayList<Category>()));

		given().contentType(ContentType.TEXT).body("Lodging").post(BASE_URL + "admin/cdl/cat");

		JSONObject actual = new JSONObject(get(BASE_URL + "user/cdl").asString());
		JSONObject expected = new JSONObject()
		.put("name", "Home")
		.put("subcategories", subcategories);

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=3)
	public void testCreateFoodCategory() throws JSONException {
		Reporter.log("3. The directory structure is \"Home\" with subcategories \"Lodging\" and \"Food.\"");

		ArrayList<JSONObject> subcategories = new ArrayList<JSONObject>();
		subcategories.add(new JSONObject()
		.put("name", "Lodging")
		.put("subcategories", new ArrayList<Category>()));
		subcategories.add(new JSONObject()
		.put("name", "Food")
		.put("subcategories", new ArrayList<Category>()));

		given().contentType(ContentType.TEXT).body("Food").post(BASE_URL + "admin/cdl/cat");

		JSONObject actual = new JSONObject(get(BASE_URL + "user/cdl").asString());
		JSONObject expected = new JSONObject()
		.put("name", "Home")
		.put("subcategories", subcategories);

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=4)
	public void testGoToLodging() throws JSONException {
		Reporter.log("4. The CDL is \"Home -> Lodging.\"");

		JSONObject expected = new JSONObject()
		.put("name", "Lodging")
		.put("subcategories", new ArrayList<Category>());

		JSONObject actual = new JSONObject(given().contentType(ContentType.TEXT)
				.body("Lodging").put(BASE_URL + "user/cdl").asString());

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=5)
	public void testAddBBBAdvertiser() throws JSONException {
		Reporter.log("5. The CDL of \"Home -> Lodging\" has the advertiser \"BBB.\"");

		JSONObject bbb = new JSONObject()
		.put("name", "BBB")
		.put("logo", "BBB_logo");

		UUID.fromString(given().contentType(ContentType.JSON).body(bbb.toString()).post(BASE_URL + "ad/cdl/ad").asString());

		JSONObject advertiser = (JSONObject)(new JSONArray(get(BASE_URL + "admin/cdl/ad").asString()).get(0));

		Assert.assertEquals(advertiser.getString("name"), "BBB");
		Assert.assertEquals(advertiser.getString("logo"), "BBB_logo");
	}

	@Test(priority=6)
	public void testAddBBBSponsor() throws JSONException {
		Reporter.log("6. The CDL of \"Home -> Lodging\" has the sponsor \"BBB.\"");

		JSONObject bbb = new JSONObject()
		.put("name", "BBB")
		.put("logo", "BBB_logo");

		UUID.fromString(given().contentType(ContentType.JSON).body(bbb.toString()).post(BASE_URL + "ad/cdl/spon").asString());

		JSONObject sponsor = (JSONObject)(new JSONArray(get(BASE_URL + "admin/cdl/spon").asString()).get(0));

		Assert.assertEquals(sponsor.getString("name"), "BBB");
		Assert.assertEquals(sponsor.getString("logo"), "BBB_logo");
	}

	@Test(priority=7)
	public void testGoToParentHome() throws JSONException {
		Reporter.log("7. The CDL is \"Home.\"");

		ArrayList<JSONObject> subcategories = new ArrayList<JSONObject>();
		subcategories.add(new JSONObject()
		.put("name", "Lodging")
		.put("subcategories", new ArrayList<Category>()));
		subcategories.add(new JSONObject()
		.put("name", "Food")
		.put("subcategories", new ArrayList<Category>()));

		JSONObject expected = new JSONObject()
		.put("name", "Home")
		.put("subcategories", subcategories);

		JSONObject actual = new JSONObject(given().put(BASE_URL + "user/cdl/parent").asString());

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	private UUID bbbUid;

	@Test(priority=8)
	public void testAddBBBSponsorHome() throws JSONException {
		Reporter.log("8. The CDL of \"Home\" has the sponsor \"BBB.\"");

		JSONObject BBB = new JSONObject()
		.put("name", "BBB")
		.put("logo", "BBB_logo");

		bbbUid = UUID.fromString(given().contentType(ContentType.JSON)
				.body(BBB.toString()).post(BASE_URL + "ad/cdl/spon").asString());

		JSONObject sponsor = (JSONObject)(new JSONArray(get(BASE_URL + "admin/cdl/spon").asString()).get(0));

		Assert.assertEquals(sponsor.getString("name"), "BBB");
		Assert.assertEquals(sponsor.getString("logo"), "BBB_logo");
	}

	@Test(priority=9)
	public void testBBBTotalDues() throws JSONException {
		Reporter.log("9. The advertiser \"BBB\" has basic listing, category sponsorship, and home sponsorship dues.");

		JSONObject advertiser = new JSONObject(get(BASE_URL + "admin/ad/" + bbbUid.toString()).asString());

		Assert.assertEquals(advertiser.getDouble("dues"), Advertiser.BASIC_LISTING_PRICE 
				+ Advertiser.CATEGORY_SPONSORSHIP_PRICE + Advertiser.HOME_PAGE_SPONSORSHIP_PRICE, AdvertiserTest.ERROR_MARGIN);
	}

	private UUID marksUid;

	@Test(priority=10)
	public void testAddMarksSponsorHome() throws JSONException {
		Reporter.log("10. The CDL of \"Home\" has the sponsor \"Mark's.\"");

		JSONObject marks = new JSONObject()
		.put("name", "Mark's")
		.put("logo", "Marks_logo");

		marksUid = UUID.fromString(given().contentType(ContentType.JSON).body(marks.toString()).post(BASE_URL + "ad/cdl/spon").asString());

		JSONObject sponsor = (JSONObject)(new JSONArray(get(BASE_URL + "admin/cdl/spon").asString()).get(1));

		Assert.assertEquals(sponsor.getString("name"), "Mark's");
		Assert.assertEquals(sponsor.getString("logo"), "Marks_logo");
	}

	@Test(priority=11)
	public void testGoToLodgingAndCheckMarksInFeatAds() throws JSONException {
		Reporter.log("11. The CDL of \"Home -> Lodging\" has the advertiser \"Mark's\" in the featured ads.");

		JSONObject expected = new JSONObject()
		.put("name", "Mark's")
		.put("logo", "Marks_logo");

		given().contentType(ContentType.TEXT).body("Lodging").put(BASE_URL + "user/cdl").asString();
		JSONObject actual = (JSONObject)(new JSONArray(get(BASE_URL + "user/cdl/featads").asString()).get(0));

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=12)
	public void testLodgingHasBBBInAds() throws JSONException {
		Reporter.log("12. The CDL of \"Home -> Lodging\" has the advertiser \"BBB\" in the ads.");

		JSONObject expected = new JSONObject()
		.put("name", "BBB");

		JSONObject actual = (JSONObject)(new JSONArray(get(BASE_URL + "user/cdl/ads").asString()).get(0));

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=13)
	public void testDirectoryTree() throws JSONException {
		Reporter.log("13. The directory tree is \"Home\" with subcategories \"Lodging\" and \"Food.\"");

		ArrayList<JSONObject> subcategories = new ArrayList<JSONObject>();
		subcategories.add(new JSONObject()
		.put("name", "Lodging"));
		subcategories.add(new JSONObject()
		.put("name", "Food"));

		JSONObject expected = new JSONObject()
		.put("category", new JSONObject()
		.put("name", "Home")
		.put("subcategories", subcategories));

		JSONObject actual = XML.toJSONObject(get(BASE_URL + "admin/tree").asString());

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=14)
	public void testBBBAndMarksInAllAdvertisers() throws JSONException {
		Reporter.log("14. The advertisers \"BBB\" and \"Mark's\" are the only advertisers in the directory.");

		JSONArray advertisers = new JSONArray(get(BASE_URL + "admin/ad").asString());
		JSONObject actualBBB = (JSONObject)advertisers.get(0);
		JSONObject actualMarks = (JSONObject)advertisers.get(1);

		Assert.assertEquals(actualBBB.getString("name"), "BBB");
		Assert.assertEquals(actualBBB.getString("logo"), "BBB_logo");
		Assert.assertEquals(actualBBB.getDouble("dues"), Advertiser.BASIC_LISTING_PRICE + Advertiser.CATEGORY_SPONSORSHIP_PRICE 
				+ Advertiser.HOME_PAGE_SPONSORSHIP_PRICE, AdvertiserTest.ERROR_MARGIN);
		Assert.assertEquals(actualBBB.getInt("numAds"), 1);
		Assert.assertEquals(actualBBB.getString("uid"), bbbUid.toString());
		Assert.assertEquals(actualMarks.getString("name"), "Mark's");
		Assert.assertEquals(actualMarks.getString("logo"), "Marks_logo");
		Assert.assertEquals(actualMarks.getDouble("dues"), Advertiser.HOME_PAGE_SPONSORSHIP_PRICE, AdvertiserTest.ERROR_MARGIN);
		Assert.assertEquals(actualMarks.getInt("numAds"), 0);
		Assert.assertEquals(actualMarks.get("uid"), marksUid.toString());
		Assert.assertEquals(advertisers.length(), 2);
	}

	@Test(priority=15)
	public void testGoToHomeAndRemoveLodging() throws JSONException {
		Reporter.log("15. The \"Lodging\" category is removed.");

		ArrayList<JSONObject> subcategories = new ArrayList<JSONObject>();
		subcategories.add(new JSONObject()
		.put("name", "Food")
		.put("subcategories", new ArrayList<Category>()));

		JSONObject expected = new JSONObject()
		.put("name", "Home")
		.put("subcategories", subcategories);

		given().put(BASE_URL + "user/cdl/parent");
		given().delete(BASE_URL + "admin/cdl/cat/Lodging");
		JSONObject actual = new JSONObject(get(BASE_URL + "user/cdl").asString());

		Assert.assertEquals(actual.toString(), expected.toString());
	}

	@Test(priority=16)
	public void testBBBDuesAdjusted() throws JSONException {
		Reporter.log("16. The dues for \"BBB\" are now just the home sponsorship price.");

		JSONObject advertiser = new JSONObject(get(BASE_URL + "admin/ad/" + bbbUid.toString()).asString());

		Assert.assertEquals(advertiser.getDouble("dues"), Advertiser.HOME_PAGE_SPONSORSHIP_PRICE, AdvertiserTest.ERROR_MARGIN);
	}

	@Test(priority=17)
	public void testRemoveMarksHomeSponsor() throws JSONException {
		Reporter.log("17. The advertiser \"Mark's\" no longer appears as an advertiser in the directory.");

		given().delete(BASE_URL + "admin/cdl/spon/" + marksUid.toString());

		JSONArray advertisers = new JSONArray(get(BASE_URL + "admin/ad").asString());
		JSONObject actualBBB = (JSONObject)advertisers.get(0);

		Assert.assertEquals(actualBBB.getString("name"), "BBB");
		Assert.assertEquals(actualBBB.getString("logo"), "BBB_logo");
		Assert.assertEquals(actualBBB.getDouble("dues"), Advertiser.HOME_PAGE_SPONSORSHIP_PRICE, AdvertiserTest.ERROR_MARGIN);
		Assert.assertEquals(actualBBB.getInt("numAds"), 0);
		Assert.assertEquals(actualBBB.getString("uid"), bbbUid.toString());
	}

	@Test(priority=18)
	public void testGoToFoodAndAddBBBAdvertiser() throws JSONException {
		Reporter.log("18. The CDL of \"Home -> Food\" has the advertiser \"BBB.\"");

		JSONObject bbb = new JSONObject()
		.put("name", "BBB")
		.put("logo", "BBB_logo");

		given().contentType(ContentType.TEXT).body("Food").put(BASE_URL + "user/cdl");
		given().contentType(ContentType.JSON).body(bbb.toString()).post(BASE_URL + "ad/cdl/ad").asString();

		JSONObject advertiser = (JSONObject)(new JSONArray(get(BASE_URL + "admin/cdl/ad").asString()).get(0));

		Assert.assertEquals(advertiser.getString("name"), "BBB");
		Assert.assertEquals(advertiser.getString("logo"), "BBB_logo");
		Assert.assertEquals(advertiser.getDouble("dues"), 
				Advertiser.BASIC_LISTING_PRICE + Advertiser.HOME_PAGE_SPONSORSHIP_PRICE);
	}

	@Test(priority=19)
	public void testGoHomeAndRemoveBBBHomeSponsor() throws JSONException {
		Reporter.log("19. The CDL of \"Home\" has no sponsors");

		given().put(BASE_URL + "user/cdl/parent");
		given().delete(BASE_URL + "admin/cdl/spon/" + bbbUid.toString());
		JSONArray sponsors = new JSONArray(get(BASE_URL + "admin/cdl/spon").asString());

		Assert.assertEquals(sponsors.length(), 0);
	}

	@Test(priority=20)
	public void testRemoveFoodCategory() throws JSONException {
		Reporter.log("20. The CDL of \"Home\" has no subcategories and the directory has no advertisers.");

		given().delete(BASE_URL + "admin/cdl/cat/Food").asString();

		JSONObject actual = new JSONObject(get(BASE_URL + "user/cdl").asString());
		JSONObject expected = new JSONObject()
		.put("name", "Home")
		.put("subcategories", new ArrayList<Category>());

		JSONArray advertisers = new JSONArray(get(BASE_URL + "admin/ad").asString());

		Assert.assertEquals(actual.toString(), expected.toString());
		Assert.assertEquals(advertisers.length(), 0);
	}
}