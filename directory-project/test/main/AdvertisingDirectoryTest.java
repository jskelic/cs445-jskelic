package main;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.UUID;
import main.AdvertisingDirectory.*;
import org.junit.Before;
import org.junit.Test;

public class AdvertisingDirectoryTest {
	private AdvertisingDirectory advertisingDirectory;

	@Before
	public void before() {
		advertisingDirectory = new AdvertisingDirectory();
	}

	@Test
	public void testGetHomeCategory() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		assertEquals(new Category(null, "Home"), advertisingDirectory.getHomeCategory());
	}

	@Test
	public void testGetAdvertiser() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		Advertiser a1 = new Advertiser("A1", "");
		advertisingDirectory.addAvertiser(a1);
		assertEquals(a1, advertisingDirectory.getAdvertiser(a1.getUid()));
	}

	@Test()
	public void testGetAdvertiserNotExists() throws Exception {
		assertNull(advertisingDirectory.getAdvertiser(UUID.randomUUID()));
	}

	@Test
	public void testGoToSubcategory() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		assertEquals(new Category(new Category(null, "Home"), "A"), advertisingDirectory.getCurrentCategory());
	}

	@Test(expected=NoSuchCategoryException.class)
	public void testGoToSubcategoryNotExists() throws Exception {
		advertisingDirectory.goToSubcategory("A");
	}

	@Test
	public void testGoToParentCategory() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		advertisingDirectory.goToParentCategory();
		assertEquals(new Category(null, "Home"), advertisingDirectory.getCurrentCategory());
	}

	@Test(expected=NoSuchCategoryException.class)
	public void testGoToParentCategoryNotExists() throws Exception {
		advertisingDirectory.goToParentCategory();
	}

	@Test
	public void testAddSubcategory() throws Exception {
		advertisingDirectory.addSubcategory("A");
		Category a = new Category(new Category(null, "Home"), "A");
		assertTrue(advertisingDirectory.getCurrentCategory().getSubcategories().contains(a));
	}

	@Test
	public void testDeleteSubcategory() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.deleteSubcategory("A");
		assertEquals(new ArrayList<Category>(), advertisingDirectory.getCurrentCategory().getSubcategories());
	}

	@Test(expected=NoSuchCategoryException.class)
	public void testDeleteSubcategoryNotExists() throws Exception {
		advertisingDirectory.deleteSubcategory("A");
	}

	@Test
	public void testDeleteSubcategoryContainsAdvertiserMoreThanOneAd() {
		Advertiser a1 = new Advertiser("A1", "");

		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.addSubcategory("B");
		advertisingDirectory.goToSubcategory("A");
		advertisingDirectory.addAvertiser(a1);
		advertisingDirectory.goToParentCategory();
		advertisingDirectory.goToSubcategory("B");
		advertisingDirectory.addAvertiser(a1);
		advertisingDirectory.goToParentCategory();
		advertisingDirectory.deleteSubcategory("A");

		assertEquals(Advertiser.BASIC_LISTING_PRICE, a1.getDues(), AdvertiserTest.ERROR_MARGIN);
	}

	@Test
	public void testDeleteSubcategoryContainsSponsorOneAd() {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		advertisingDirectory.goToParentCategory();
		advertisingDirectory.deleteSubcategory("A");

		assertEquals(advertisingDirectory.getAllAdvertisers().size(), 0);
	}

	@Test
	public void testAddAdvertiser() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		Advertiser a1 = new Advertiser("A1", "");
		advertisingDirectory.addAvertiser(a1);
		assertTrue(advertisingDirectory.getCurrentCategory().getAdvertisers().contains(a1));
	}

	@Test(expected=CannotAdvertiseHomePageException.class)
	public void testAddAdvertiserHome() throws Exception {
		advertisingDirectory.addAvertiser(new Advertiser("A1", ""));
	}

	@Test
	public void testAddAdvertiserTwiceSameCredentials() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.addSubcategory("B");
		advertisingDirectory.goToSubcategory("A");
		Advertiser addedAdvertiser = advertisingDirectory.addAvertiser(new Advertiser("A1", ""));
		advertisingDirectory.goToParentCategory();
		advertisingDirectory.goToSubcategory("B");
		Advertiser addedAdvertiserAgain = advertisingDirectory.addAvertiser(new Advertiser("A1", ""));
		assertEquals(addedAdvertiser, addedAdvertiserAgain);
	}

	@Test
	public void testDeleteAdvertiserMoreThanOneAd() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.addSubcategory("B");
		advertisingDirectory.goToSubcategory("A");
		advertisingDirectory.addAvertiser(new Advertiser("A1", ""));
		advertisingDirectory.goToParentCategory();
		advertisingDirectory.goToSubcategory("B");
		Advertiser a1 = advertisingDirectory.addAvertiser(new Advertiser("A1", ""));
		advertisingDirectory.deleteAdvertiser(a1.getUid());
		assertEquals(a1, advertisingDirectory.getAdvertiser(a1.getUid()));
	}

	@Test(expected=NoSuchAdvertiserException.class)
	public void testDeleteAdvertiserNotExists() throws Exception {
		advertisingDirectory.deleteAdvertiser(UUID.randomUUID());
	}

	@Test
	public void testDeleteAdvertiserOneAd() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		Advertiser a1 = advertisingDirectory.addAvertiser(new Advertiser("A1", ""));
		advertisingDirectory.deleteAdvertiser(a1.getUid());
		assertNull(advertisingDirectory.getAdvertiser(a1.getUid()));
	}

	@Test
	public void testAddSponsorNotHome() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		Advertiser a1 = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		assertTrue(advertisingDirectory.getCurrentCategory().getSponsors().contains(a1));
	}

	@Test
	public void testAddSponsorHome() throws Exception {
		Advertiser a1 = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		assertTrue(advertisingDirectory.getCurrentCategory().getSponsors().contains(a1));
	}

	@Test
	public void testAddSponsorOverMaxNotHome() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		for(int i = 0; i < AdvertisingDirectory.MAX_SPONSORS_PER_CATEGORY; i++)
			advertisingDirectory.addSponsor(new Advertiser("A" + i, ""));
		assertNull(advertisingDirectory.addSponsor(new Advertiser("A0", "")));
	}

	@Test
	public void testAddSponsorOverMaxHome() throws Exception {
		for(int i = 0; i < AdvertisingDirectory.MAX_SPONSORS_FOR_HOME_PAGE; i++)
			advertisingDirectory.addSponsor(new Advertiser("A" + i, ""));
		assertNull(advertisingDirectory.addSponsor(new Advertiser("A0", "")));
	}

	@Test
	public void testAddSponsorTwiceSameCredentials() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		Advertiser addedSponsor = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		advertisingDirectory.goToParentCategory();
		Advertiser addedSponsorAgain = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		assertEquals(addedSponsor, addedSponsorAgain);
	}

	@Test
	public void testDeleteSponsorNotHomeMoreThanOneAd() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.addSubcategory("B");
		advertisingDirectory.goToSubcategory("A");
		advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		advertisingDirectory.goToParentCategory();
		advertisingDirectory.goToSubcategory("B");
		Advertiser a1 = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		advertisingDirectory.deleteSponsor(a1.getUid());
		assertEquals(a1, advertisingDirectory.getAdvertiser(a1.getUid()));
	}

	@Test
	public void testDeleteSponsorNotHomeOneAd() throws Exception {
		advertisingDirectory.addSubcategory("A");
		advertisingDirectory.goToSubcategory("A");
		Advertiser a1 = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		advertisingDirectory.deleteSponsor(a1.getUid());
		assertNull(advertisingDirectory.getAdvertiser(a1.getUid()));
	}

	@Test
	public void testDeleteSponsorHome() throws Exception {
		Advertiser a1 = advertisingDirectory.addSponsor(new Advertiser("A1", ""));
		advertisingDirectory.deleteSponsor(a1.getUid());
		assertNull(advertisingDirectory.getAdvertiser(a1.getUid()));
	}

	@Test(expected=NoSuchSponsorException.class)
	public void testDeleteSponsorNotExists() throws Exception {
		advertisingDirectory.deleteSponsor(UUID.randomUUID());
	}
}