package main;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.HashSet;
import main.Category.*;
import org.junit.Before;
import org.junit.Test;

public class CategoryTest {
	private Category category;

	@Before
	public void before() {
		category = new Category(null, "Home");
	}

	@Test
	public void testGetFeaturedAdvertisers() throws Exception {
		Advertiser a1 = new Advertiser("A1", "");
		Advertiser a2 = new Advertiser("A2", "");
		Advertiser a3 = new Advertiser("A3", "");

		category.addSponsor(a1);
		category = new Category(category, "A");
		category.addSponsor(a2);
		category.addSponsor(a3);

		HashSet<Advertiser> expected = new HashSet<Advertiser>();
		expected.add(a1);
		expected.add(a2);
		expected.add(a3);

		assertTrue(category.getFeaturedAdvertisers().containsAll(expected));
		assertTrue(expected.containsAll(category.getFeaturedAdvertisers()));
	}

	@Test
	public void testAddSubcategory() throws Exception {
		category.addSubcategory("A");
		category.addSubcategory("B");
		category.addSubcategory("C");

		ArrayList<Category> expected = new ArrayList<Category>();
		expected.add(new Category(category, "A"));
		expected.add(new Category(category, "B"));
		expected.add(new Category(category, "C"));

		assertTrue(category.getSubcategories().containsAll(expected));
		assertTrue(expected.containsAll(category.getSubcategories()));
	}

	@Test(expected=SubcategoryAlreadyExistsException.class)
	public void testAddSubcategoryAlreadyExists() throws Exception {
		category.addSubcategory("A");
		category.addSubcategory("A");
	}

	@Test
	public void testDeleteSubcategory() throws Exception {
		category.addSubcategory("A");
		category.addSubcategory("B");
		category.addSubcategory("C");
		category.deleteSubcategory("C");

		ArrayList<Category> expected = new ArrayList<Category>();
		expected.add(new Category(category, "B"));
		expected.add(new Category(category, "A"));

		assertTrue(category.getSubcategories().containsAll(expected));
		assertTrue(expected.containsAll(category.getSubcategories()));
	}

	@Test
	public void testDeleteSubcategoryNotExists() throws Exception {
		assertNull(category.deleteSubcategory("A"));
	}

	@Test
	public void testAddAdvertiser() throws Exception {
		category = new Category(category, "A");

		Advertiser a1 = new Advertiser("A1", "");
		Advertiser a2 = new Advertiser("A2", "");
		Advertiser a3 = new Advertiser("A3", "");

		category.addAdvertiser(a1);
		category.addAdvertiser(a2);
		category.addAdvertiser(a3);

		ArrayList<Advertiser> expected = new ArrayList<Advertiser>();
		expected.add(a1);
		expected.add(a3);
		expected.add(a2);

		assertTrue(category.getAdvertisers().containsAll(expected));
		assertTrue(expected.containsAll(category.getAdvertisers()));
	}

	@Test(expected=AdvertiserAlreadyExistsException.class)
	public void testAddAdvertiserAlreadyExists() throws Exception {
		category = new Category(category, "A");
		category.addAdvertiser(new Advertiser("A1", ""));
		category.addAdvertiser(new Advertiser("A1", ""));
	}

	@Test
	public void testDeleteAdvertiser() throws Exception {
		category = new Category(category, "A");

		category.addAdvertiser(new Advertiser("A1", ""));
		category.addAdvertiser(new Advertiser("A2", ""));
		category.addAdvertiser(new Advertiser("A3", ""));
		category.deleteAdvertiser(new Advertiser("A1", ""));

		ArrayList<Advertiser> expected = new ArrayList<Advertiser>();
		expected.add(new Advertiser("A3", ""));
		expected.add(new Advertiser("A2", ""));

		assertTrue(category.getAdvertisers().containsAll(expected));
		assertTrue(expected.containsAll(category.getAdvertisers()));
	}

	@Test
	public void testDeleteAdvertiserNotExists() throws Exception {
		assertNull(category.deleteAdvertiser(new Advertiser("A1", "")));
	}

	@Test
	public void testAddSponsor() throws Exception {
		Advertiser a1 = new Advertiser("A1", "");
		Advertiser a2 = new Advertiser("A2", "");
		Advertiser a3 = new Advertiser("A3", "");

		category.addSponsor(a1);
		category.addSponsor(a2);
		category.addSponsor(a3);

		HashSet<Advertiser> expected = new HashSet<Advertiser>();
		expected.add(a1);
		expected.add(a2);
		expected.add(a3);

		assertTrue(category.getSponsors().containsAll(expected));
		assertTrue(expected.containsAll(category.getSponsors()));
	}

	@Test(expected=SponsorAlreadyExistsException.class)
	public void testAddSponsorAlreadyExists() throws Exception {
		category.addSponsor(new Advertiser("A1", ""));
		category.addSponsor(new Advertiser("A1", ""));
	}

	@Test
	public void testDeleteSponsor() throws Exception {
		category.addSponsor(new Advertiser("A1", ""));
		category.addSponsor(new Advertiser("A2", ""));
		category.addSponsor(new Advertiser("A3", ""));
		category.deleteSponsor(new Advertiser("A1", ""));

		ArrayList<Advertiser> expected = new ArrayList<Advertiser>();
		expected.add(new Advertiser("A3", ""));
		expected.add(new Advertiser("A2", ""));

		assertTrue(category.getSponsors().containsAll(expected));
		assertTrue(expected.containsAll(category.getSponsors()));
	}

	@Test
	public void testDeleteSponsorNotExists() throws Exception {
		assertNull(category.deleteSponsor(new Advertiser("A1", "")));
	}
}