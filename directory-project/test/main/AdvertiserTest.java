package main;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class AdvertiserTest {
	public static final double ERROR_MARGIN = 0.000001;
	private Advertiser advertiser;

	@Before
	public void before() {
		advertiser = new Advertiser("name", "");
	}

	@Test
	public void testAddAdvertiserAmountFirstTimeAddsBasicAmount() throws Exception {
		advertiser.addAdvertiserAmount();
		assertEquals(Advertiser.BASIC_LISTING_PRICE, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testAddAdvertiserAmountAgainAddsAdditionalAmount() throws Exception {
		for(int i = 0; i < 10; i++)
			advertiser.addAdvertiserAmount();
		assertEquals(Advertiser.BASIC_LISTING_PRICE + 9*Advertiser.ADDITIONAL_LISTING_PRICE, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testSubtractAdvertiserAmountWithOneAdSubtractsBasicAmount() throws Exception {
		advertiser.addAdvertiserAmount();
		advertiser.subtractAdvertiserAmount();
		assertEquals(0, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testSubtractAdvertiserAmountWithMoreThanOneAdSubtractsAdditionalAmount() throws Exception {
		for(int i = 0; i < 10; i++)
			advertiser.addAdvertiserAmount();
		advertiser.subtractAdvertiserAmount();
		assertEquals(Advertiser.BASIC_LISTING_PRICE + 8*Advertiser.ADDITIONAL_LISTING_PRICE, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testAddSponsorAmount() throws Exception {
		advertiser.addSponsorAmount();
		assertEquals(Advertiser.CATEGORY_SPONSORSHIP_PRICE, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testSubtractSponsorAmount() throws Exception {
		advertiser.addSponsorAmount();
		advertiser.subtractSponsorAmount();
		assertEquals(0, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testAddHomeSponsorAmount() throws Exception {
		advertiser.addHomeSponsorAmount();
		assertEquals(Advertiser.HOME_PAGE_SPONSORSHIP_PRICE, advertiser.getDues(), ERROR_MARGIN);
	}

	@Test
	public void testSubtractHomeSponsorAmount() throws Exception {
		advertiser.addHomeSponsorAmount();
		advertiser.subtractHomeSponsorAmount();
		assertEquals(0, advertiser.getDues(), ERROR_MARGIN);
	}
}